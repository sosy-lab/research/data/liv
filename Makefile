# requires the folders sv-benchmarks and svcomp22-witnesses from the SV-COMP 2022 archives
#OPTIONS = --cloud --cloudPrio HIGH --cloudCPU 1230 --read-only-dir / --full-access-dir . --overlay-dir /home
#CLOUDMASTER = --cloudMaster vcloud-master.sosy.ifi.lmu.de
BENCHMARKCOMMAND = benchexec
#OPTIONS = --read-only-dir / --overlay-dir /home/stephen/extdisk -N2 -c1
CLOUDMASTER =
VMNAME = liv

svcomp22-witnesses.zip:
	wget "https://zenodo.org/record/5838498/files/svcomp22-witnesses.zip?download=1" -O svcomp22-witnesses.zip

sv-benchmarks-svcomp22.zip:
	wget "https://zenodo.org/record/5831003/files/svcomp22-testcomp22-sv-benchmarks.zip?download=1" -O sv-benchmarks-svcomp22.zip

sv-benchmarks: sv-benchmarks-svcomp22.zip
	if [ ! -d sv-benchmarks ] ; then unzip sv-benchmarks-svcomp22.zip ; fi

svcomp22-witnesses: svcomp22-witnesses.zip
	if [ ! -d svcomp22-witnesses ] ; then ./setup_witness_files.sh ; fi

liv:
	git clone https://gitlab.com/sosy-lab/software/liv
	git -C liv/lib clone https://gitlab.com/sosy-lab/software/coveriteam
	git -C liv/lib/coveriteam reset --hard 76a8e4dba327af4606e2a30544656b7882f75848
	git -C liv/lib clone --branch 3.15 https://github.com/sosy-lab/benchexec
	git -C liv/lib clone --branch pycparser-fixes https://github.com/MartinSpiessl/pycparserext.git
	curl -L "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cbmc.zip" -o cbmc.zip &&  unzip cbmc.zip -d "liv/lib" && rm cbmc.zip
	mkdir liv/lib/cvt_cache
	mkdir liv/lib/python
	curl -L "https://files.pythonhosted.org/packages/00/e5/f12a80907d0884e6dff9c16d0c0114d81b8cd07dc3ae54c5e962cc83037e/tqdm-4.66.1-py3-none-any.whl" -o liv/lib/python/tqdm-4.66.1-py3-none-any.whl
	curl -L "https://github.com/sosy-lab/benchexec/releases/download/3.15/BenchExec-3.15-py3-none-any.whl" -o liv/lib/python/BenchExec-3.15-py3-none-any.whl
	cp test-sets/*.xml liv/test/test-sets
	cp -r exp liv/test/programs/exp
	make example && make examplecbmc && make examplecpalia && rm -rf liv/output liv/cvt-output liv/lastexecution # fill cvt-cache with tools by running examples, clean up afterwards

cpachecker:
	curl -L "https://gitlab.com/sosy-lab/sv-comp/archives-2022/-/raw/main/2022/cpachecker.zip" -o cpachecker.zip &&  unzip cpachecker.zip  && rm cpachecker.zip

experiment1:
	export PYTHONPATH="."; cd liv; lib/benchexec/bin/benchexec test/test-sets/zilu.xml  --overlay-dir /home/ --read-only-dir / --full-access-dir  /sys/fs/cgroup -o ../results_experiment1/

experiment2-liv:
	export PYTHONPATH="."; cd liv; lib/benchexec/bin/benchexec test/test-sets/livexperiments.xml  --overlay-dir /home/ --read-only-dir / --full-access-dir  /sys/fs/cgroup -o ../results_experiment2/

experiment2-cpachecker:
	cd CPAchecker-2.1-unix; ../liv/lib/benchexec/bin/benchexec ../liv/test/test-sets/cpacheckerexperiments.xml --overlay-dir /home/ --read-only-dir / --full-access-dir  /sys/fs/cgroup -o ../results_experiment2

experiment2: experiment2-liv experiment2-cpachecker


example:
	cd liv; bin/liv --witness test/programs/simple/example.yml.yaml test/programs/simple/example.i --property test/properties/unreach-call.prp

examplecbmc:
	cd liv; bin/liv --witness test/programs/simple/example.yml.yaml test/programs/simple/example.i --property test/properties/unreach-call.prp --verifier actors/cbmc.yml --verifierversion default

examplecpalia:
	cd liv; bin/liv --witness test/programs/simple/example.yml.yaml test/programs/simple/example.i --property test/properties/unreach-call.prp --verifier actors/cpachecker.yml --verifierversion lia

examplestats:
	cd liv; bin/liv --witness test/programs/simple/example.yml.yaml test/programs/simple/example.i --property test/properties/unreach-call.prp --verifier actors/cbmc.yml --verifierversion default --stats

exampleverbose:
	cd liv; bin/liv --witness test/programs/simple/example.yml.yaml test/programs/simple/example.i --property test/properties/unreach-call.prp --verifier actors/cbmc.yml --verifierversion default --verbose

exampleexperiment1:
	cd liv; bin/liv --witness test/programs/loop-zilu/benchmark25_linear.yml.yaml test/programs/loop-zilu/benchmark25_linear.i --verifier actors/cbmc.yml --verifierversion default --property test/properties/unreach-call.prp


exampleexperiment2:
	cd liv; bin/liv --witness test/programs/exp/uautomizer/loop-zilu/benchmark25_linear.yml.graphml test/programs/exp/uautomizer/loop-zilu/benchmark25_linear.i --verifier actors/cbmc.yml --verifierversion default --property test/properties/unreach-call.prp

artifact.ova: Vagrantfile
	vagrant destroy -f
	rm -rf liv && make liv
	rm -rf CPAchecker-2.1-unix && make cpachecker
	rm -f artifact.ova
	vagrant up
	vagrant halt
	vboxmanage sharedfolder remove ${VMNAME} --name vagrant
	./compact.sh
	vboxmanage export ${VMNAME} -o artifact.ova
	vagrant destroy -f
