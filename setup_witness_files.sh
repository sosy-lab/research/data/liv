#!/bin/bash

mkdir witnesseszip
fuse-zip svcomp22-witnesses.zip witnesseszip

# copy the two smaller folders as they are:
cp -r witnesseszip/svcomp22-witnesses/witnessInfoByHash svcomp22-witnesses/witnessInfoByHash
cp -r witnesseszip/svcomp22-witnesses/witnessListByProgramHashJSON svcomp22-witnesses/witnessListByProgramHashJSON

# copy toplevel regular files
find witnesseszip/svcomp22-witnesses -maxdepth 1 -not -type d -exec bash -c 'cp {} svcomp22-witnesses/$(basename {})' \;

# copy the witnesses over by compressing them with gzip
find witnesseszip/svcomp22-witnesses/fileByHash/ -type f -exec bash -c 'gzip -c {} > svcomp22-witnesses/fileByHash/$(basename {}).gz' \;
# parallel version:
#find witnesseszip/svcomp22-witnesses/fileByHash/ -type f -print0 | parallel -0 bash -c 'gzip -c {} > svcomp22-witnesses2/fileByHash/$(basename {}).gz'
#| xargs --null --max-args=1 --max-procs=$(nproc) gzip -9 --force
fusermount -u witnesseszip
rmdir witnesseszip
