#!/bin/bash
set -xe
VMNAME="liv"
VMDKPATH=$(vboxmanage showvminfo $VMNAME  --machinereadable |grep SCSI-0-0 | cut -d "=" -f 2 | tr -d '"')
VDIPATH=$(dirname $VMDKPATH)/$(basename $VMDKPATH .vmdk).vdi
echo "Mounting existing disk image and applying zerofree...."
mkdir -p mnt
qemu-nbd --socket $PWD/socket.sock $VMDKPATH --cache=unsafe --discard=unmap &
nbdfuse mnt/nbd1 --command nbdkit -s nbd socket=socket.sock --filter=partition partition=1 &
sleep 3
zerofree -v mnt/nbd1
fusermount -u ./mnt
rmdir mnt
echo "done"
echo "Cloning disk"
vboxmanage clonehd $(vboxmanage showvminfo $VMNAME  --machinereadable |grep SCSI-0-0 | cut -d "=" -f 2 | tr -d '"') $VDIPATH --format vdi
echo "Attaching new vdi disk instead of the vmdk disk"
vboxmanage storageattach $VMNAME --storagectl "SCSI" --device 0 --port 0 --type hdd --medium $VDIPATH
echo "Deleting old vmdk disk"
vboxmanage closemedium $VMDKPATH --delete
