# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.box = "ubuntu/jammy64"

  name = "liv"

  config.vm.provider "virtualbox" do |vb|
    vb.gui = true # change this to false if you want a headless VM
    vb.cpus = "2" # change this for a different number of vCPUs on the VM
    vb.memory = "10000" # change this to adapt the RAM available to the VM (in MB)
    vb.name = name # change this to use a different name for the VM
  end

  config.vm.provision "shell", inline: <<-SHELL
    sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config # allow login via password for convenience
    apt-get update
    apt-get install -y ant gcc openjdk-17-jdk-headless make unzip python3-pip python3-virtualenv
    wget --quiet https://github.com/sosy-lab/benchexec/releases/download/3.11/benchexec_3.11-2_all.deb
    dpkg -i benchexec_3.11-2_all.deb
    rm benchexec_3.11-2_all.deb
    adduser vagrant benchexec
    echo 'GRUB_CMDLINE_LINUX_DEFAULT="${GRUB_CMDLINE_LINUX_DEFAULT} systemd.unified_cgroup_hierarchy=0"' | tee /etc/default/grub.d/cgroupsv1-for-benchexec.cfg
    update-grub
  SHELL

  config.vm.provision :reload # requires `vagrant plugin install vagrant-reload`

  config.vm.provision "repository", type: "shell", privileged: false, inline: <<-SHELL
    rm -rf #{name}
    cp -r /vagrant #{name}
    virtualenv -p python3 #{name}/liv/.venv
    . #{name}/liv/.venv/bin/activate && pip3 install -r #{name}/liv/requirements.txt
  SHELL

  #config.vm.provision "standalone", type: "shell", privileged:false, run: "never", inline: <<-SHELL
  #  cd #{name}
  #  make ...
  #SHELL
end
