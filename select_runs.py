import gzip
import json
import os
import pickle
import shutil

SVBENCHMARKS = "./sv-benchmarks/"
SVCOMPRESULTS = "./svcomp22-results/"
SVWITNESSES = "./svcomp22-witnesses/"

runs = pickle.load(open("runs.pickle", "rb"))

verifiers = set([run["verifier"] for run in runs.values()])
print(verifiers)
import sys


def select(verifier):
    result = [
        run
        for name, run in runs.items()
        if verifier in run["verifier"]
        and run["taskname"].startswith("loop-zilu/")
        and run["property"] == "unreach-call"
        and "witness-info" in run
        and run["witness-info"]["witness-type"] == "correctness_witness"
    ]
    print(
        f"We found {len(result)} reachability correctness witnesses from {verifier} belonging to tasks in the loop-zilu/ folder of sv-benchmarks"
    )
    return result


selected = dict()
for verifier in verifiers:
    result = select(verifier)
    if result:
        selected[verifier] = result

print(json.dumps(selected["cpachecker"][0], sort_keys=True, indent=4))

def replace(filename, old, new):
    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0)
        f.write(content.replace(old, new))
        f.truncate()

def prepare(taskname, witnesshash, verifier):
    newdir = f"exp/{verifier}/{os.path.dirname(taskname)}"
    os.makedirs(newdir, exist_ok=True)
    basename = os.path.basename(taskname)[:-4]
    dirname = os.path.dirname(taskname)
    prefix = verifier + "."
    for base in [f"{basename}.{ext}" for ext in {"c", "i", "yml"}]:
        src = f"{SVBENCHMARKS}/c/{dirname}/{base}"
        dst = f"{newdir}/{prefix}{base}"
        if os.path.exists(src):
            shutil.copy(src=src, dst=dst)
        else:
            assert not src.endswith(".yml"), "task definition file missing!"
        if base.endswith("yml"):
            replace(dst, f'{basename}.i', f"{prefix}{basename}.i")
            replace(dst, f'{basename}.c', f"{prefix}{basename}.c")
            replace(dst, "../properties", "../../../../properties")
    with gzip.open(f"{SVWITNESSES}fileByHash/{witnesshash}.graphml.gz", "rb") as f_in:
        with open(f"{newdir}/{prefix}{basename}.yml.graphml", "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)
    with open(f"exp/{verifier}.set", "w") as setfile:
        setfile.write(f"{verifier}/*/*.yml")


for verifier in selected.keys():
    for run in selected[verifier]:
        prepare(run["taskname"], run["witness-sha256"], verifier)
