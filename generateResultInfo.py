#!/usr/bin/env python3
import argparse
import csv
import glob
import hashlib
import json
import os
import pickle
import re
import random
import subprocess
import sys
import progressbar # pip3 install progressbar2

from benchexec import result, tablegenerator
from benchexec.tablegenerator import util
from datetime import datetime
from shutil import copyfile
from os.path import basename,isfile
from os import makedirs

SVBENCHMARKS="./sv-benchmarks"
SVCOMPRESULTS = "./svcomp22-results/"
SVWITNESSES = "./svcomp22-witnesses/"

def process(fname):
    """
    extract runs from a result xml
    """
    import logging
    #table-generator warns about empty run sets, we don't care:
    logging.disable(logging.WARNING)
    res = tablegenerator.RunSetResult.create_from_xml(fname,tablegenerator.parse_results_file(fname))
    res.collect_data(False)
    runs = list()
    for run in res.results:
        d = dict()
        for c,v in zip(run.columns,run.values):
            d[c.title] = v
        d["taskname"] = os.path.relpath(run.task_id.name,SVCOMPRESULTS + "/sv-benchmarks/c/") #give short path inside c/ folder
        if not run.task_id.expected_result:
            assert "Collatz" in run.task_id.name # Collatz is only task without an expected verdict in SVCOMP 21
            continue
        else:
            #d["status"] = run.status
            d["expected_result"] = run.task_id.expected_result.result
            if run.task_id.expected_result.subproperty:
                d["expected_result_subproperty"] = run.task_id.expected_result.subproperty
        d["statuscategory"] = run.category
        runs.append(d)
    logging.disable(logging.NOTSET)
    return runs

def decompose_validatorxml_name(fname):
    m = re.match("([a-z0-9-]*)-validate-(violation|correctness)?-?witnesses-([a-z0-9-]*)\.[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}-[0-9]{2}-[0-9]{2}\.results\.SV-COMP[0-9][0-9]_([a-z-]*)\.?([a-zA-Z0-9]*)?-?([a-zA-Z0-9-]*)?.xml.bz2",fname)
    if m:
        d=dict()
        d["validator"]=m[1]
        d["verifier"]=m[3]
        d["property"]=m[4]
        d["category"]=m[5]
        d["subcategory"]=m[6]
        return d
    else:
        return None

def decompose_verifierxml_name(fname):
    m = re.match("([a-z0-9-]*)\.[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}-[0-9]{2}-[0-9]{2}\.results\.SV-COMP[0-9][0-9]_([a-z-]*)\.?([a-zA-Z0-9]*)?-?([a-zA-Z0-9-]*)?\.xml\.bz2\.merged\.xml\.bz2$",fname)
    if m:
        d=dict()
        d["verifier"]=m[1]
        d["property"]=m[2]
        d["category"]=m[3]
        d["subcategory"]=m[4]
        return d
    else:
        return None

def verifier_run_key_basic(verifier,property_,taskname):
    return f'{verifier}_{property_}_{taskname}'
def verifier_run_key(run):
    return f'{run["verifier"]}_{run["property"]}_{run["taskname"]}'
def make_hashes(filename):
    if filename in make_hashes.cache:
        return make_hashes.cache[filename]
    m1 = hashlib.sha1()
    m256 = hashlib.sha256()
    content = open(filename,"rb").read()
    m1.update(content)
    m256.update(content)
    hash1=m1.hexdigest()
    hash256 = m256.hexdigest()
    make_hashes.cache[filename] = (hash1,hash256)
    return (hash1,hash256)
make_hashes.cache = dict()

def getProgramNameFromRun(run):
    taskname = run["taskname"]
    if taskname in getProgramNameFromRun.cache:
        return getProgramNameFromRun.cache[taskname]
    ymlpath = SVBENCHMARKS+"/c/"+taskname
    import yaml
    y = yaml.load(open(ymlpath,"r").read(),Loader=yaml.FullLoader)
    programname = y["input_files"]
    programpath = SVBENCHMARKS+"/c/"+os.path.dirname(taskname)+"/"+programname
    assert os.path.isfile(programpath)
    getProgramNameFromRun.cache[taskname] = programpath
    return programpath
getProgramNameFromRun.cache = dict()
def getProgramHashFromRun(run):
    return make_hashes(getProgramNameFromRun(run))

class PickleCache:
    def __init__(self, filename, compute):
        self.filename = filename
        self.compute = compute
    def store(self, value):
        with open(self.filename, 'wb') as handle:
            pickle.dump(value, handle, protocol=pickle.HIGHEST_PROTOCOL)
    def load(self):
        if os.path.isfile(self.filename):
            with open(self.filename, 'rb') as handle:
                return pickle.load(handle)
        result =  self.compute()
        self.store(result)
        return result
           

# ------------------------------------------------------------- READ IN WITNESS INFOS

def compute_witnessInfoByHash():
    witnessInfoByHash= dict()
    for filepath in progressbar.progressbar(list(glob.iglob(SVWITNESSES + '/witnessInfoByHash/*.json')), prefix = "Reading witnessInfoByHash: "):
        witness = json.loads(open(filepath,"r").read())
        witnessInfoByHash[witness["witness-sha256"]]=witness
    return witnessInfoByHash
witnessInfoByHash = PickleCache('witnessInfoByHash.pickle', compute_witnessInfoByHash).load()
print("Successfully read %d witnessInfoByHash json files" % len(witnessInfoByHash))

# ------------------------------------------------------------- READ IN VALIDATOR RUNS AND VERIFIER RUNS
    
def compute_validatorruns():
    validatorruns = list()
    for fname in progressbar.progressbar(list(glob.iglob(SVCOMPRESULTS + "results-validated/*.xml*")), prefix = "Reading validator results: "):
        d = decompose_validatorxml_name(os.path.basename(fname))
        if not d:
            if not "assert_java" in fname:
                print("Unable to decompose validatorxml for %s " % fname)
            continue
        if not d["subcategory"]:
            continue # filter out the summarized xmls that contain all subcategories
        runs = process(fname)
        for run in runs:
            run["taskcategory"] = d["category"]
            run["tasksubcategory"] = d["subcategory"]
            run["property"] = d["property"]
            run["verifier"] = d["verifier"]
            run["validator"] = d["validator"]
        validatorruns.extend(runs)
    return validatorruns
validatorruns = PickleCache('validatorruns.pickle', compute_validatorruns).load()
print(f"We read a total of {len(validatorruns)} validator runs")

def compute_verifierruns():
    verifierruns = list()
    for fname in progressbar.progressbar(list(glob.iglob(SVCOMPRESULTS + "results-verified/*.xml*")), prefix = "Reading verifier results"):
        d = decompose_verifierxml_name(os.path.basename(fname))
        if not d or not d["subcategory"]:
            continue # filter out the summarized xmls that contain all subcategories
        runs = process(fname)
        for run in runs:
            run["taskcategory"] = d["category"]
            run["tasksubcategory"] = d["subcategory"]
            run["property"] = d["property"]
            run["verifier"] = d["verifier"]
        verifierruns.extend(runs)
    return verifierruns
verifierruns = PickleCache('verifierruns.pickle', compute_verifierruns).load()
print(f"We read a total of {len(verifierruns)} verifier runs")

# ------------------------------------------------------------- MAKE HASHDICT TO REFERENCE VERIFIER RUNS MORE EFFICIENTLY
def compute_augmentation():
    verifierdict = dict()
    full_taskname = dict()
    for run in progressbar.progressbar(verifierruns,prefix="Augmenting verifier runs"):
        # some lookup information to map tasknames later (fileHash stores only contain the basename, we still want to map them to the full name)
        full_taskname[os.path.basename(run["taskname"])] = run["taskname"]
        # augment entries with information about the actual program file
        run["programfile"] = getProgramNameFromRun(run)
        hash1, hash256 = getProgramHashFromRun(run)
        run["programhash"] = hash256
        run["programhash_sha1"] = hash1
        # transfer runs into a dictionary such that we can look them up in O(1) later when adding validator runs
        key = verifier_run_key(run)
        verifierdict[key] = run
    return (verifierdict, full_taskname)
verifierdict, full_taskname = PickleCache('augmentation.pickle', compute_augmentation).load()


# ADD THE VALIDATOR RUNS TO EACH MATCHING VERIFIER RUN USING THE UNIQUE KEY:
for validatorrun in validatorruns:
    key = verifier_run_key(validatorrun)
    if not key in verifierdict:
        continue
    if not "validations" in verifierdict[key] or isinstance(verifierdict[key]["validations"],list):
        verifierdict[key]["validations"] = dict()
    validator = validatorrun["validator"]
    #print(validator)
    #print(type(verifierdict[key]["validations"]))
    verifierdict[key]["validations"][validator] = validatorrun

# -------------------------------------------------------------

#from unpackFileStores import parse as parseFileStore #gives import problems in jupyter
import gzip
def parseFileStore(json_file):
    if json_file.endswith(".gz"):
        file_open = gzip.open
    else:
        file_open = open
    with file_open(json_file) as inp:
        return json.load(inp)

removed = set()
from collections import Counter
missing = Counter()
def add_witness_hashes():
    for fileStore in glob.iglob(SVCOMPRESULTS+"results-verified/*fileHashes.json.gz"):
        basename = os.path.basename(fileStore)
        m = re.match("([a-z0-9-]*)\..*fileHashes.json.gz",basename)
        if not m or len(m.groups())<1:
            print(m.groups())
            print(basename)
        verifier = m.groups()[0]
        store = parseFileStore(fileStore)
        witnesses = {k:v for k,v in store.items() if k.endswith("graphml") and os.path.dirname(k).endswith(".yml")}
        print(f"{verifier}: {len(witnesses)}")
        for k,v in witnesses.items():
            taskdir = os.path.basename(os.path.dirname(k))
            runsetdir = os.path.basename(os.path.dirname(os.path.dirname(k)))
            assert taskdir.endswith(".yml")
            m = re.match("SV-COMP[0-9][0-9]_([a-z-]*)",runsetdir)
            property_ = m.groups()[0]
            m = re.match("(.*.yml)",taskdir)
            short_taskname = m.groups()[0]
            if not short_taskname in full_taskname:
                removed.add(short_taskname)
                continue
            taskname = full_taskname[short_taskname]
            key = verifier_run_key_basic(verifier,property_,taskname)
            if not key in verifierdict:
                assert "no-data-race" in key or "busybox-1.22.0" in key or "jbmc" in key , "%s " % key
                continue
            witnesshash = v
            if not witnesshash in witnessInfoByHash:
                #for k,v in verifierdict[key].items():
                #    print(f"{k}: {v}")
                missing[verifier] +=1
                print("Missing witnessInfoByHash for witness with hash: %s" % witnesshash)
                continue
            witness = witnessInfoByHash[witnesshash]
            verifierdict[key]["witness-sha256"] = witnesshash
            verifierdict[key]["witness-info"] = witness
            #print(v)
            #break
    return verifierdict
print("we found %d tasks to be removed" % len(removed))
print("the following numbers of witness metainformations are missing:")
print(missing)

verifierdict = PickleCache('runs.pickle', add_witness_hashes).load()

# ------------------------------------------------------------- WRITE RESULTS

import json
from os import makedirs
makedirs("./rundata",exist_ok=True)
for k,v in progressbar.progressbar(list(verifierdict.items()), prefix = "Writing task information to rundata: "):
    name = f'{v["verifier"]}_{v["property"]}_{os.path.basename(v["taskname"])}'
    with open("./rundata/"+name+".json", 'w') as fp:
        json.dump(v, fp, indent = 4)
